import { GameSleeper, Menu as MenuSDK } from "wrapper/Imports"

export enum LanePicker {
	NONE = 0,
	HARD,
	MID,
	EASY,
	JUNGLE,
	JUNGLE_ENEMY,
}

export class HERO_SELECTION {

	public static Sleeper = new GameSleeper()
	public static PossibleHeroBool: boolean = false
	public static HERO_BANNED_OR_SELECTED: number[] = []

	public static get Language() {
		return new Map([
			["Heroes", "Герои"],
			["Strength", "Сила"],
			["Agility", "Ловкость"],
			["Auto Pick", "Авто Пик"],
			["Auto Ban", "Авто Бан"],
			["Intellect", "Интеллект"],
			["Primary attribute hero", "Главный атрибут героя"],
		])
	}

	public static PossibleHeroBoolReset = () => {
		if (HERO_SELECTION.PossibleHeroBool)
			HERO_SELECTION.PossibleHeroBool = false
		if (HERO_SELECTION.Sleeper.Sleeping("LANE_MARKER"))
			HERO_SELECTION.Sleeper.ResetKey("LANE_MARKER")
	}

	public static Dispose() {
		this.Sleeper.FullReset()
		this.PossibleHeroBool = false
		this.HERO_BANNED_OR_SELECTED = []
	}
}

MenuSDK.Localization.AddLocalizationUnit("russian", HERO_SELECTION.Language)
