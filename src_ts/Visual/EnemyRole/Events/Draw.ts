import { EventsSDK, GameState, LocalPlayer, Team } from "wrapper/Imports"
import { ROLES_DATA } from "../data"
import { DRAW_ROLES } from "../Service/Render"
import { ROLES_VALIDATE } from "../Service/Validate"

EventsSDK.on("Draw", () => {
	if (
		LocalPlayer === undefined
		|| LocalPlayer.PlayerID === -1
		|| !ROLES_VALIDATE.IsPreGame
	)
		return

	const my_team = GameState.LocalTeam
	if (my_team !== Team.Radiant && my_team !== Team.Dire)
		return

	const enemy_team = my_team !== Team.Radiant ? 0 : 1
	ROLES_DATA.Roles[enemy_team].forEach((role, i) => {
		if (role !== undefined)
			DRAW_ROLES.Roles(i, role)
	})
})
