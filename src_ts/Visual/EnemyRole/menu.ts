import { Menu as MenuSDK } from "wrapper/Imports"

const Menu = MenuSDK.AddEntryDeep(["Visual", "Enemies Roles"])
export const RolesState = Menu.AddToggle("State", true, "Show/turn off roles enemies")
export const RolesTextSize = Menu.AddSlider("Text size", 14, 12, 22)
export const RolesDrawMode = Menu.AddDropdown("Draw mode", ["Icons", "Text"], 1)

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Enemies Roles", "Роли врагов"],
	["Draw mode", "Режим отображения"],
	["Icons", "Иконки"],
	["Show/turn off roles enemies", "Показать/выключить роли врагов"],
	["Support", "Поддержка"],
	["Safe Lane", "Лёгкая"],
	["Off Lane", "Сложная"],
	["Mid Lane", "Мид"],
]))
