import { Color, EntityManager, EventsSDK, GameRules, GameState, Hero, LocalPlayer, Menu as MenuSDK, RendererSDK, RenderMode_t, Team, Vector2, vengefulspirit_command_aura } from "wrapper/Imports"

const Menu = MenuSDK.AddEntryDeep(["Visual", "Show Illusions"])
const stateMain = Menu.AddToggle("State", true)
const MenuTreeInvis = Menu.AddNode("Invisible illusions", undefined, "Invisible illusions & draw circle position")
const deleteIllusion = MenuTreeInvis.AddToggle("State", false)
const MenuColor = Menu.AddColorPicker("Color", new Color(0, 0, 160))

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["Show Illusions", "Отображение иллюзий"],
	["Invisible illusions", "Невидимые иллюзии"],
	["Invisible illusions & draw circle position", "и рисует окружность на их позиции\nДанная опция сделает иллюзии не видимыми"],
]))

let FilterHeroes: Hero[] = []

function IsValid() {
	return stateMain.value
		&& GameRules?.IsInGame
		&& LocalPlayer !== undefined
		&& GameState.LocalTeam !== Team.Observer
}

EventsSDK.on("Tick", () => {
	if (!IsValid())
		return
	FilterHeroes = EntityManager.GetEntitiesByClass(Hero).filter(hero =>
		hero.IsIllusion
		&& hero.IsEnemy()
		&& hero.IsAlive
		&& hero.IsVisible,
	)
})

EventsSDK.on("Draw", () => {
	if (!IsValid())
		return

	FilterHeroes.forEach(hero => {
		const deleteThisIllusion = (
			deleteIllusion.value
			&& (hero.GetAbilityByClass(vengefulspirit_command_aura)?.Level ?? 0) === 0
		)
		if (deleteThisIllusion) {
			const pos = RendererSDK.WorldToScreen(hero.Position)
			if (pos !== undefined) {
				const vector = new Vector2(30, 30)
				RendererSDK.FilledCircle(pos, vector, Color.Yellow.SetA(190))
				RendererSDK.OutlinedCircle(pos, vector, Color.Orange.SetA(190))
			}
		}

		hero.CustomDrawColor = [
			MenuColor.selected_color,
			!deleteThisIllusion
				? RenderMode_t.kRenderTransColor
				: RenderMode_t.kRenderNone,
		]
	})
})
