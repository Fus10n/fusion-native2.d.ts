import { EventsX } from "X-Core/Imports"
import { ItemPanelData } from "../data"

EventsX.on("GameEnded", () => {
	ItemPanelData.Dispose()
})
